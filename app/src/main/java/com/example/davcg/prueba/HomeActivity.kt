package com.example.davcg.prueba

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button

class HomeActivity: AppCompatActivity() {
    internal lateinit var individual: Button
    internal lateinit var vsAmigo: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity)

        individual = findViewById(R.id.individualButton)
        vsAmigo = findViewById(R.id.vsAmigoButton)
individual.setOnClickListener {
    val intento1 = Intent(this, SeleccionarLetraIndividualActivity::class.java)


    startActivity(intento1)
}
        vsAmigo.setOnClickListener {
    val intento1 = Intent(this, JoinCreateActivity::class.java)


    startActivity(intento1)
}
}

    }
