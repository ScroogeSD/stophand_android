package com.example.davcg.prueba

import android.content.Intent
import android.os.Bundle
import android.renderscript.Sampler
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.google.android.gms.internal.measurement.zzsl.init
import com.google.firebase.FirebaseError
import com.google.firebase.database.*

import com.google.firebase.database.DatabaseError
import java.nio.file.Files.exists
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.ValueEventListener



class EsperaLetraActivity:AppCompatActivity() {
    internal lateinit var jugar: Button
internal lateinit var letraT:TextView
    companion object {
        val INTENT_Player2 = "playerId"
        val INTENT_Room = "room"
        val INTENT_Letra ="letra"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var letra=""
        var notificado =""
        val database = FirebaseDatabase.getInstance()
        val playerId2 = "player2"
        val room= intent.getStringExtra(UnirseActivity.INTENT_Room)
        val roomRecovered= intent.getStringExtra(PuntajesP2Activity.room)

        val ref = database.getReference("stopHand/" + "juego/"+room+"/" )

        ref.child("letra").setValue("null")
        ref.child("parameLaMano2").setValue("null")
        ref.child("notificado?").setValue("null")
        ref.child("parameLaMano1").setValue("null")


        setContentView(R.layout.esperando_letra)
        jugar = findViewById(R.id.jugarButton)
        letraT = findViewById(R.id.letraTextView)
        jugar.isEnabled= false

        val eventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                 letra = dataSnapshot.child("letra").value.toString()

              letraT.text= letra

                if (letraT.text === "null"){

                }
                else {
                    notificado = "true"
                    ref.child("notificado?").setValue(notificado)

                    jugar.isEnabled= true
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        }
        ref.addValueEventListener(eventListener)

        jugar.setOnClickListener {
            if(room.isNullOrEmpty()){
                enviar(letra,playerId2,roomRecovered)
            }
            else {
                enviar(letra, playerId2, room)
            }
        }

    }

    private fun enviar(letra:String, id:String, room:String){
        val intento1 = Intent(this, JuegoP2Activity::class.java)
        intento1.putExtra(EsperaLetraActivity.INTENT_Player2,id )
        intento1.putExtra(EsperaLetraActivity.INTENT_Room, room)
        intento1.putExtra(EsperaLetraActivity.INTENT_Letra, letra)
        startActivity(intento1)

    }

}