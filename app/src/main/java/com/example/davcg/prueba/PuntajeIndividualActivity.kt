package com.example.davcg.prueba

import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.util.*

class PuntajeIndividualActivity : AppCompatActivity() {
    internal lateinit var player1: TextView
    internal lateinit var player2: TextView
    internal lateinit var nombrePlayer1: TextView
    internal lateinit var nombrePlayer2: TextView
    internal lateinit var paisPlayer1: TextView
    internal lateinit var paisPlayer2: TextView
    internal lateinit var ciudadPlayer1: TextView
    internal lateinit var ciudadPlayer2: TextView
    internal lateinit var animalPlayer1: TextView
    internal lateinit var animalPlayer2: TextView
    internal lateinit var frutaPlayer1: TextView
    internal lateinit var frutaPlayer2: TextView
    internal lateinit var puntajenombrePlayer1: TextView
    internal lateinit var puntajenombrePlayer2: TextView
    internal lateinit var puntajepaisPlayer1: TextView
    internal lateinit var puntajepaisPlayer2: TextView
    internal lateinit var puntajeciudadPlayer1: TextView
    internal lateinit var puntajeciudadPlayer2: TextView
    internal lateinit var puntajeanimalPlayer1: TextView
    internal lateinit var puntajeanimalPlayer2: TextView
    internal lateinit var puntajefrutaPlayer1: TextView
    internal lateinit var puntajefrutaPlayer2: TextView
    internal lateinit var puntajeTotalPlayer1: TextView
    internal lateinit var puntajeTotalPlayer2: TextView

    internal lateinit var salir: Button
    internal lateinit var nuevaPartida: Button
    companion object {
        val nombrePlayer = "room"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.puntajes_individual)


        val nombre = intent.getStringExtra(JuegoIndividualActivity.nombrePlayer)
        val ciudad = intent.getStringExtra(JuegoIndividualActivity.ciudadPlayer)
        val pais = intent.getStringExtra(JuegoIndividualActivity.paisPlayer)
        val animal = intent.getStringExtra(JuegoIndividualActivity.animalPlayer)
        val fruta = intent.getStringExtra(JuegoIndividualActivity.frutaPlayer)


        val letra = intent.getStringExtra(JuegoIndividualActivity.letra)

        val database = FirebaseDatabase.getInstance()

        val ref = database.getReference("stopHand/")
        salir = findViewById(R.id.salirButton)
        nuevaPartida = findViewById(R.id.nuevaPartidaButton)

        player1 = findViewById(R.id.player1TextView)
        player2 = findViewById(R.id.player2TextView)
        nombrePlayer1 = findViewById(R.id.nombrePlayer1TextView)
        nombrePlayer2 = findViewById(R.id.nombrePlayer2TextView)
        paisPlayer1 = findViewById(R.id.paisPlayer1TextView)
        paisPlayer2 = findViewById(R.id.paisPlayer2TextView)
        ciudadPlayer1 = findViewById(R.id.ciudadPlayer1TextView)
        ciudadPlayer2 = findViewById(R.id.ciudadPlayer2TextView)
        paisPlayer1 = findViewById(R.id.paisPlayer1TextView)
        paisPlayer2 = findViewById(R.id.paisPlayer2TextView)
        animalPlayer1 = findViewById(R.id.animalPlayer1TextView)
        animalPlayer2 = findViewById(R.id.animalPlayer2TextView)
        frutaPlayer1 = findViewById(R.id.frutaPlayer1TextView)
        frutaPlayer2 = findViewById(R.id.frutaPlayer2TextView)

        puntajeanimalPlayer1 = findViewById(R.id.puntajeAnimalPlayer1)
        puntajeanimalPlayer2 = findViewById(R.id.puntajeAnimalPlayer2)
        puntajepaisPlayer1 = findViewById(R.id.puntajePaisPlayer1)
        puntajepaisPlayer2 = findViewById(R.id.puntajePaisPlayer2)
        puntajeciudadPlayer1 = findViewById(R.id.puntajeCiudadPlayer1)
        puntajeciudadPlayer2 = findViewById(R.id.puntajeCiudadPlayer2)
        puntajenombrePlayer1 = findViewById(R.id.puntajeNombrePlayer1)
        puntajenombrePlayer2 = findViewById(R.id.puntajeNombrePlayer2)
        puntajefrutaPlayer1 = findViewById(R.id.puntajeFrutaPlayer1)
        puntajefrutaPlayer2 = findViewById(R.id.puntajeFrutaPlayer2)
        puntajeTotalPlayer1 = findViewById(R.id.puntajeTotalPlayer1TextView)
        puntajeTotalPlayer2 = findViewById(R.id.puntajeTotalPlayer2TextView)


        player1.text = "PLAYER"
        player2.text = "CPU"

        val eventListener = object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {

                var nombreCPU =dataSnapshot.child("palabras").child(letra).child("Nombres").value.toString()
                nombreCPU=nombreCPU.replace("{","").replace("}","")

                val nombresSep= nombreCPU.split(",", "=")
                val random = Random()

                val indiceNombre  = random.nextInt(1..nombresSep.size)
                var nombreCPUFinal =nombresSep[indiceNombre]
                nombrePlayer2.text=nombreCPUFinal.trim()

                var ciudadCPU =dataSnapshot.child("palabras").child(letra).child("Ciudades").value.toString()
                ciudadCPU=ciudadCPU.replace("{","").replace("}","")

                val ciudadesSep= ciudadCPU.split(",", "=")

                val indiceCiudad  = random.nextInt(1..ciudadesSep.size)
                var ciudadCPUFinal =ciudadesSep[indiceCiudad]
                ciudadPlayer2.text=ciudadCPUFinal.trim()


                               var paisCPU =dataSnapshot.child("palabras").child(letra).child("Paises").value.toString()
                paisCPU=paisCPU.replace("{","").replace("}","")

                val paisesSep= paisCPU.split(",", "=")

                val indicePais  = random.nextInt(1..paisesSep.size)
                var paisCPUFinal =paisesSep[indicePais]
                paisPlayer2.text=paisCPUFinal.trim()

                               var animalCPU =dataSnapshot.child("palabras").child(letra).child("Animales").value.toString()
                animalCPU=animalCPU.replace("{","").replace("}","")

                val animalesSep= animalCPU.split(",", "=")

                val indiceAnimal  = random.nextInt(1..animalesSep.size)
                var animalCPUFinal =animalesSep[indiceAnimal]
                animalPlayer2.text=animalCPUFinal.trim()

                var frutaCPU =dataSnapshot.child("palabras").child(letra).child("Frutas").value.toString()
                frutaCPU=frutaCPU.replace("{","").replace("}","")

                val frutasSep= frutaCPU.split(",", "=")

                val indiceFruta  = random.nextInt(1..frutasSep.size)
                var frutaCPUFinal =frutasSep[indiceFruta]
                frutaPlayer2.text=frutaCPUFinal.trim()

                nombrePlayer1.text = nombre
                ciudadPlayer1.text = ciudad
                paisPlayer1.text= pais
                animalPlayer1.text=animal
                frutaPlayer1.text=fruta




                if (nombrePlayer1.text.isNullOrEmpty()) {
                    puntajenombrePlayer1.text = 0.toString()
                    puntajenombrePlayer1.setTextColor(Color.parseColor("#da0d00"))
                }
                else {
                    if (dataSnapshot.child("palabras").child(letra).child("Nombres")
                            .child(nombrePlayer1.text.toString().trim().toUpperCase()).value.toString() != "null"
                    ) {


                        if (nombrePlayer1.text.toString().trim().toUpperCase() !=
                            nombrePlayer2.text.toString().trim().toUpperCase()
                        ) {
                            puntajenombrePlayer1.text = 100.toString()
                            puntajenombrePlayer1.setTextColor(Color.parseColor("#10bd00"))

                        } else {
                            puntajenombrePlayer1.text = 50.toString()
                            puntajenombrePlayer1.setTextColor(Color.parseColor("#dabd00"))

                        }

                    } else {
                        puntajenombrePlayer1.text = 0.toString()
                        puntajenombrePlayer1.setTextColor(Color.parseColor("#da0d00"))

                    }
                }
                if (nombrePlayer2.text.isNullOrEmpty()) {
                    puntajenombrePlayer2.text = 0.toString()
                    puntajenombrePlayer2.setTextColor(Color.parseColor("#da0d00"))
                }else {
                    if (dataSnapshot.child("palabras").child(letra).child("Nombres")
                            .child(nombrePlayer2.text.toString().trim().toUpperCase()).value.toString() != "null"
                    ) {


                        if (nombrePlayer1.text.toString().trim().toUpperCase() !=
                            nombrePlayer2.text.toString().trim().toUpperCase()
                        ) {
                            puntajenombrePlayer2.text = 100.toString()
                            puntajenombrePlayer2.setTextColor(Color.parseColor("#10bd00"))
                        } else {
                            puntajenombrePlayer2.text = 50.toString()
                            puntajenombrePlayer2.setTextColor(Color.parseColor("#dabd00"))
                        }

                    } else {
                        puntajenombrePlayer2.text = 0.toString()
                        puntajenombrePlayer2.setTextColor(Color.parseColor("#da0d00"))
                    }
                }



                if (ciudadPlayer1.text.isNullOrEmpty()){
                    puntajeciudadPlayer1.text=0.toString()
                    puntajeciudadPlayer1.setTextColor(Color.parseColor("#da0d00"))

                }else {
                    if (dataSnapshot.child("palabras").child(letra).child("Ciudades")
                            .child(ciudadPlayer1.text.toString().trim().toUpperCase()).value.toString() != "null"
                    ) {


                        if (ciudadPlayer1.text.toString().trim().toUpperCase() !=
                            ciudadPlayer2.text.toString().trim().toUpperCase()
                        ) {
                            puntajeciudadPlayer1.text = 100.toString()
                            puntajeciudadPlayer1.setTextColor(Color.parseColor("#10bd00"))
                        } else {
                            puntajeciudadPlayer1.text = 50.toString()
                            puntajeciudadPlayer1.setTextColor(Color.parseColor("#dabd00"))

                        }

                    } else {
                        puntajeciudadPlayer1.text = 0.toString()
                        puntajeciudadPlayer1.setTextColor(Color.parseColor("#da0d00"))

                    }
                }
                if (ciudadPlayer2.text.isNullOrEmpty()){
                    puntajeciudadPlayer2.text=0.toString()
                    puntajeciudadPlayer2.setTextColor(Color.parseColor("#da0d00"))

                }
                else {
                    if (dataSnapshot.child("palabras").child(letra).child("Ciudades")
                            .child(ciudadPlayer2.text.toString().trim().toUpperCase()).value.toString() != "null"
                    ) {


                        if (ciudadPlayer1.text.toString().trim().toUpperCase() !=
                            ciudadPlayer2.text.toString().trim().toUpperCase()
                        ) {

                            puntajeciudadPlayer2.text = 100.toString()
                            puntajeciudadPlayer2.setTextColor(Color.parseColor("#10bd00"))
                        } else {
                            puntajeciudadPlayer2.text = 50.toString()
                            puntajeciudadPlayer2.setTextColor(Color.parseColor("#dabd00"))

                        }

                    } else {
                        puntajeciudadPlayer2.setTextColor(Color.parseColor("#da0d00"))
                        puntajeciudadPlayer2.text = 0.toString()

                    }
                }



                if (paisPlayer1.text.isNullOrEmpty()){
                    puntajepaisPlayer1.text=0.toString()
                    puntajepaisPlayer1.setTextColor(Color.parseColor("#da0d00"))

                }else {
                    if (dataSnapshot.child("palabras").child(letra).child("Paises")
                            .child(paisPlayer1.text.toString().trim().toUpperCase()).value.toString() != "null"
                    ) {


                        if (paisPlayer1.text.toString().trim().toUpperCase() !=
                            paisPlayer2.text.toString().trim().toUpperCase()
                        ) {
                            puntajepaisPlayer1.text = 100.toString()
                            puntajepaisPlayer1.setTextColor(Color.parseColor("#10bd00"))
                        } else {
                            puntajepaisPlayer1.text = 50.toString()
                            puntajepaisPlayer1.setTextColor(Color.parseColor("#dabd00"))

                        }

                    } else {
                        puntajepaisPlayer1.text = 0.toString()
                        puntajepaisPlayer1.setTextColor(Color.parseColor("#da0d00"))

                    }

                }
                if (paisPlayer2.text.isNullOrEmpty()){
                    puntajepaisPlayer2.text=0.toString()
                    puntajepaisPlayer2.setTextColor(Color.parseColor("#da0d00"))

                }else {
                    if (dataSnapshot.child("palabras").child(letra).child("Paises")
                            .child(paisPlayer2.text.toString().trim().toUpperCase()).value.toString() != "null"
                    ) {


                        if (paisPlayer1.text.toString().trim().toUpperCase() !=
                            paisPlayer2.text.toString().trim().toUpperCase()
                        ) {


                            puntajepaisPlayer2.setTextColor(Color.parseColor("#10bd00"))
                            puntajepaisPlayer2.text = 100.toString()
                        } else {
                            puntajepaisPlayer2.text = 50.toString()
                            puntajepaisPlayer2.setTextColor(Color.parseColor("#dabd00"))

                        }

                    } else {
                        puntajepaisPlayer2.text = 0.toString()
                        puntajepaisPlayer2.setTextColor(Color.parseColor("#da0d00"))

                    }
                }



                if (animalPlayer1.text.isNullOrEmpty()){
                    puntajeanimalPlayer1.text=0.toString()
                    puntajeanimalPlayer1.setTextColor(Color.parseColor("#da0d00"))

                }else {
                    if (dataSnapshot.child("palabras").child(letra).child("Animales")
                            .child(animalPlayer1.text.toString().trim().toUpperCase()).value.toString() != "null"
                    ) {


                        if (animalPlayer1.text.toString().trim().toUpperCase() !=
                            animalPlayer2.text.toString().trim().toUpperCase()
                        ) {
                            puntajeanimalPlayer1.text = 100.toString()
                            puntajeanimalPlayer1.setTextColor(Color.parseColor("#10bd00"))
                        } else {
                            puntajeanimalPlayer1.text = 50.toString()
                            puntajeanimalPlayer1.setTextColor(Color.parseColor("#dabd00"))

                        }

                    } else {
                        puntajeanimalPlayer1.text = 0.toString()
                        puntajeanimalPlayer1.setTextColor(Color.parseColor("#da0d00"))

                    }
                }
                if (animalPlayer2.text.isNullOrEmpty()){
                    puntajeanimalPlayer2.text=0.toString()
                    puntajeanimalPlayer2.setTextColor(Color.parseColor("#da0d00"))

                }else {
                    if (dataSnapshot.child("palabras").child(letra).child("Animales")
                            .child(animalPlayer2.text.toString().trim().toUpperCase()).value.toString() != "null"
                    ) {


                        if (animalPlayer1.text.toString().trim().toUpperCase() !=
                            animalPlayer2.text.toString().trim().toUpperCase()
                        ) {
                            puntajeanimalPlayer2.text = 100.toString()
                            puntajeanimalPlayer2.setTextColor(Color.parseColor("#10bd00"))
                        } else {
                            puntajeanimalPlayer2.text = 50.toString()
                            puntajeanimalPlayer2.setTextColor(Color.parseColor("#dabd00"))
                        }

                    } else {
                        puntajeanimalPlayer2.text = 0.toString()
                        puntajeanimalPlayer2.setTextColor(Color.parseColor("#da0d00"))
                    }
                }



                if (frutaPlayer1.text.isNullOrEmpty()){
                    puntajefrutaPlayer1.text=0.toString()
                    puntajefrutaPlayer1.setTextColor(Color.parseColor("#da0d00"))

                }else {
                    if (dataSnapshot.child("palabras").child(letra).child("Frutas")
                            .child(frutaPlayer1.text.toString().trim().toUpperCase()).value.toString() != "null"
                    ) {


                        if (animalPlayer1.text.toString().trim().toUpperCase() !=
                            animalPlayer2.text.toString().trim().toUpperCase()
                        ) {
                            puntajefrutaPlayer1.text = 100.toString()
                            puntajefrutaPlayer1.setTextColor(Color.parseColor("#10bd00"))

                        } else {
                            puntajefrutaPlayer1.text = 50.toString()
                            puntajefrutaPlayer1.setTextColor(Color.parseColor("#dabd00"))
                        }

                    } else {
                        puntajefrutaPlayer1.text = 0.toString()
                        puntajefrutaPlayer1.setTextColor(Color.parseColor("#da0d00"))
                    }
                }
                if (frutaPlayer2.text.isNullOrEmpty()){
                    puntajefrutaPlayer2.text=0.toString()
                    puntajefrutaPlayer2.setTextColor(Color.parseColor("#da0d00"))

                } else {
                    if (dataSnapshot.child("palabras").child(letra).child("Frutas")
                            .child(frutaPlayer2.text.toString().trim().toUpperCase()).value.toString() != "null"
                    ) {


                        if (animalPlayer1.text.toString().trim().toUpperCase() !=
                            animalPlayer2.text.toString().trim().toUpperCase()
                        ) {
                            puntajefrutaPlayer2.text = 100.toString()
                            puntajefrutaPlayer2.setTextColor(Color.parseColor("#10bd00"))
                        } else {
                            puntajefrutaPlayer2.text = 50.toString()
                            puntajefrutaPlayer2.setTextColor(Color.parseColor("#dabd00"))
                        }

                    } else {
                        puntajefrutaPlayer2.text = 0.toString()
                        puntajefrutaPlayer2.setTextColor(Color.parseColor("#da0d00"))
                    }
                }

                puntajeTotalPlayer1.text = (puntajenombrePlayer1.text.toString().toInt() +
                        puntajeciudadPlayer1.text.toString().toInt() +
                        puntajepaisPlayer1 .text.toString().toInt() +
                        puntajeanimalPlayer1.text.toString().toInt() +
                        puntajefrutaPlayer1.text.toString().toInt() ).toString()

                puntajeTotalPlayer2.text = (puntajenombrePlayer2.text.toString().toInt() +
                        puntajeciudadPlayer2.text.toString().toInt() +
                        puntajepaisPlayer2 .text.toString().toInt() +
                        puntajeanimalPlayer2.text.toString().toInt() +
                        puntajefrutaPlayer2.text.toString().toInt() ).toString()




            }
        }
        ref.addValueEventListener(eventListener)
        nuevaPartida.setOnClickListener {
            val intento1 = Intent(this, SeleccionarLetraIndividualActivity::class.java)

            ref.child("notificado?").setValue("null")

            startActivity(intento1)
        }
        salir.setOnClickListener {
            val intento1 = Intent(this, HomeActivity::class.java)

            startActivity(intento1)
        }
    }
    fun Random.nextInt(range: IntRange): Int {
        return range.start + nextInt(range.last - range.start)
    }

}