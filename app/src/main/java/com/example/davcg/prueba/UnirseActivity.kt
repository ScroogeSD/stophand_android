package com.example.davcg.prueba

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import com.google.firebase.database.FirebaseDatabase

class UnirseActivity :AppCompatActivity(){
    internal lateinit var siguiente: Button
    internal lateinit var room: EditText
    companion object {
        val INTENT_Player2 = "playerId"
        val INTENT_Room = "room"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val partidaId = intent.getStringExtra(JoinCreateActivity.INTENT_Partida);
        val database = FirebaseDatabase.getInstance()

        val ref = database.getReference("stopHand/" + "juego/")

        setContentView(R.layout.unirse_a_room)
        val playerId = intent.getStringExtra(JoinCreateActivity.INTENT_Player);
        siguiente = findViewById(R.id.siguienteButton)
        room=findViewById(R.id.codeText)




        siguiente.setOnClickListener {
            val intento1 = Intent(this, EsperaLetraActivity::class.java)
            intento1.putExtra(INTENT_Player2, "player2")
            intento1.putExtra(INTENT_Room, room.text.toString())
            startActivity(intento1)


            ref.child(room.text.toString().trim())
                .child("player2")
                .setValue(playerId.toString())


        }

    }

}