package com.example.davcg.prueba

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


class JuegoActivity : AppCompatActivity() {
    companion object {
        var puntajePlayer1 = 0

        var animalPlayer1 = ""

        var frutaPlayer1 = ""

        var paisPlayer1 = ""

        var ciudadPlayer1 = ""

        var nombrePlayer1 = ""

        var letra = ""
        var room = "room"
        var idPlayer1=""
    }

    internal lateinit var letraTextView: TextView
    internal lateinit var boton: Button
    internal lateinit var nombre: EditText
    internal lateinit var ciudad: EditText
    internal lateinit var pais: EditText
    internal lateinit var animal: EditText
    internal lateinit var fruta: EditText
    var parameLaMano = ""

    internal lateinit var countDownTimer: CountDownTimer
    internal lateinit var countDownTimer2: CountDownTimer

    internal val initialCountDown = 10000L
    internal var countDownInterval = 1000L
    internal var timeLeft = 10

    val database = FirebaseDatabase.getInstance()
    val ref = database.getReference("stopHand/" + "juego")
    var playerId1 =""
    var roomCreated =""
    internal var letraDeJuego = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.juego_activity)
        playerId1 = intent.getStringExtra(SeleccionarLetraActivity.INTENT_Player1);

         roomCreated = intent.getStringExtra(SeleccionarLetraActivity.INTENT_Room)
        val letra1 = intent.getStringExtra(SeleccionarLetraActivity.INTENT_Letter)
        val ref2 = database.getReference("stopHand/" + "juego/"+roomCreated+"/")
var stop =""
        var stop2=""
        letraTextView = findViewById(R.id.letraTextView)
        boton = findViewById(R.id.button)

        nombre = findViewById(R.id.nameText)
        ciudad = findViewById(R.id.cityText)
        pais = findViewById(R.id.countryText)
        animal = findViewById(R.id.animalText)
        fruta = findViewById(R.id.frutaText)
        letraDeJuego= letra1
        letraTextView.text = letra1

        val eventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                  stop = dataSnapshot.child("parameLaMano1").value.toString()
                  stop2 = dataSnapshot.child("parameLaMano2").value.toString()
                Log.e("stoppy", stop)
                if (stop != "null" && stop2 != "null"){

                    enviar()
                    countDownTimer.cancel()

                }
                else if (stop != "null"){
                    contar()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        }
        ref2.addValueEventListener(eventListener)
        boton.setOnClickListener {

                _ ->
            if (stop != "null"){
                enviarABase(playerId1, roomCreated)

                ref.child(roomCreated).child("parameLaMano2").setValue("true")
            }
            else {
                contar()
                enviarABase(playerId1, roomCreated)

                ref.child(roomCreated).child("parameLaMano2").setValue("true")
            }

        }

    }

    private fun enviarABase(playerId: String, room: String) {

        ref.child(room).child(playerId).child("nombre").setValue(nombre.text.toString().trim())
        ref.child(room).child(playerId).child("ciudad").setValue(ciudad.text.toString().trim())
        ref.child(room).child(playerId).child("pais").setValue(pais.text.toString().trim())
        ref.child(room).child(playerId).child("animal").setValue(animal.text.toString().trim())
        ref.child(room).child(playerId).child("fruta").setValue(fruta.text.toString().trim())
    }

    private var isCounting = false

    private fun contar() {

        timeLeft=10

        Log.d("HERE:", "Its been called more than once p1")


        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval) {
            override fun onFinish() {
                countDownTimer.cancel()
                enviarABase(playerId1, roomCreated)
                enviar()
            }

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                letraTextView.textSize=14f
                letraTextView.text = getString(R.string.time_left, Integer.toString(timeLeft))
             }
        }

        if (!isCounting) {
            countDownTimer.start()
            isCounting = true
        }
    }


    private fun enviar() {
        countDownTimer.cancel()

        val intento1 = Intent(this, PuntajesActivity::class.java)
        intento1.putExtra(nombrePlayer1, nombre.text.toString().toUpperCase().trim())
        intento1.putExtra(paisPlayer1, pais.text.toString().toUpperCase().trim())
        intento1.putExtra(ciudadPlayer1, ciudad.text.toString().toUpperCase().trim())
        intento1.putExtra(animalPlayer1, animal.text.toString().toUpperCase().trim())
        intento1.putExtra(frutaPlayer1, fruta.text.toString().toUpperCase().trim())
        intento1.putExtra(idPlayer1, playerId1)
        intento1.putExtra(room, roomCreated)
        intento1.putExtra(letra, letraDeJuego)


        startActivity(intento1)

    }
}





