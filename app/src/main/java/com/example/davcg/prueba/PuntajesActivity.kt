package com.example.davcg.prueba

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class PuntajesActivity:AppCompatActivity(){
    internal lateinit var player1: TextView
    internal lateinit var player2: TextView
    internal lateinit var nombrePlayer1: TextView
    internal lateinit var nombrePlayer2: TextView
    internal lateinit var paisPlayer1: TextView
    internal lateinit var paisPlayer2: TextView
    internal lateinit var ciudadPlayer1: TextView
    internal lateinit var ciudadPlayer2: TextView
    internal lateinit var animalPlayer1: TextView
    internal lateinit var animalPlayer2: TextView
    internal lateinit var frutaPlayer1: TextView
    internal lateinit var frutaPlayer2: TextView
    internal lateinit var puntajenombrePlayer1: TextView
    internal lateinit var puntajenombrePlayer2: TextView
    internal lateinit var puntajepaisPlayer1: TextView
    internal lateinit var puntajepaisPlayer2: TextView
    internal lateinit var puntajeciudadPlayer1: TextView
    internal lateinit var puntajeciudadPlayer2: TextView
    internal lateinit var puntajeanimalPlayer1: TextView
    internal lateinit var puntajeanimalPlayer2: TextView
    internal lateinit var puntajefrutaPlayer1: TextView
    internal lateinit var puntajefrutaPlayer2: TextView
    internal lateinit var puntajeTotalPlayer1: TextView
    internal lateinit var puntajeTotalPlayer2: TextView

    internal lateinit var salir: Button
    internal lateinit var nuevaPartida : Button

    companion object {
        val room = "room"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    setContentView(R.layout.puntajes_activity)

        val roomCreated = intent.getStringExtra(JuegoActivity.room)
        val letra = intent.getStringExtra(JuegoActivity.letra)
        val database = FirebaseDatabase.getInstance()

        val ref = database.getReference("stopHand/" )



        salir = findViewById(R.id.salirButton)
        nuevaPartida = findViewById(R.id.nuevaPartidaButton)

        player1 = findViewById(R.id.player1TextView)
        player2 = findViewById(R.id.player2TextView)
        nombrePlayer1 = findViewById(R.id.nombrePlayer1TextView)
        nombrePlayer2 = findViewById(R.id.nombrePlayer2TextView)
        paisPlayer1 = findViewById(R.id.paisPlayer1TextView)
        paisPlayer2 = findViewById(R.id.paisPlayer2TextView)
        ciudadPlayer1 = findViewById(R.id.ciudadPlayer1TextView)
        ciudadPlayer2 = findViewById(R.id.ciudadPlayer2TextView)
        paisPlayer1 = findViewById(R.id.paisPlayer1TextView)
        paisPlayer2 = findViewById(R.id.paisPlayer2TextView)
        animalPlayer1 = findViewById(R.id.animalPlayer1TextView)
        animalPlayer2 = findViewById(R.id.animalPlayer2TextView)
        frutaPlayer1 = findViewById(R.id.frutaPlayer1TextView)
        frutaPlayer2 = findViewById(R.id.frutaPlayer2TextView)

        puntajeanimalPlayer1 = findViewById(R.id.puntajeAnimalPlayer1)
        puntajeanimalPlayer2 = findViewById(R.id.puntajeAnimalPlayer2)
        puntajepaisPlayer1 = findViewById(R.id.puntajePaisPlayer1)
        puntajepaisPlayer2 = findViewById(R.id.puntajePaisPlayer2)
        puntajeciudadPlayer1 = findViewById(R.id.puntajeCiudadPlayer1)
        puntajeciudadPlayer2 = findViewById(R.id.puntajeCiudadPlayer2)
        puntajenombrePlayer1 = findViewById(R.id.puntajeNombrePlayer1)
        puntajenombrePlayer2 = findViewById(R.id.puntajeNombrePlayer2)
        puntajefrutaPlayer1 = findViewById(R.id.puntajeFrutaPlayer1)
        puntajefrutaPlayer2 = findViewById(R.id.puntajeFrutaPlayer2)
        puntajeTotalPlayer1 = findViewById(R.id.puntajeTotalPlayer1TextView)
        puntajeTotalPlayer2= findViewById(R.id.puntajeTotalPlayer2TextView)

        val playerId1 = intent.getStringExtra(JuegoActivity.idPlayer1);
        val playerId2 = intent.getStringExtra(JuegoP2Activity.idPlayer2);

        player1.text = "PLAYER 1 "
        player2.text = "PLAYER 2 "
        val eventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {



                nombrePlayer1.text = dataSnapshot.child("juego").child(roomCreated).child("player1")
                    .child("nombre").value.toString()

                nombrePlayer2.text = dataSnapshot.child("juego").child(roomCreated).child("player2")
                    .child("nombre").value.toString()

                if (nombrePlayer1.text.isNullOrEmpty()) {
                    puntajenombrePlayer1.text = 0.toString()
                    puntajenombrePlayer1.setTextColor(Color.parseColor("#da0d00"))
                }
else {
                    if (dataSnapshot.child("palabras").child(letra).child("Nombres")
                            .child(
                                dataSnapshot.child("juego").child(roomCreated).child("player1")
                                    .child("nombre").value.toString().trim().toUpperCase()
                            ).value.toString() != "null"
                    ) {


                        if (dataSnapshot.child("juego").child(roomCreated).child("player1")
                                .child("nombre").value.toString().trim().toUpperCase() !=
                            dataSnapshot.child("juego").child(roomCreated).child("player2")
                                .child("nombre").value.toString().trim().toUpperCase()
                        ) {
                            puntajenombrePlayer1.text = 100.toString()
                            puntajenombrePlayer1.setTextColor(Color.parseColor("#10bd00"))

                        } else {
                            puntajenombrePlayer1.text = 50.toString()
                            puntajenombrePlayer1.setTextColor(Color.parseColor("#dabd00"))

                        }

                    } else {
                        puntajenombrePlayer1.text = 0.toString()
                        puntajenombrePlayer1.setTextColor(Color.parseColor("#da0d00"))

                    }
                }
                if (nombrePlayer2.text.isNullOrEmpty()) {
                    puntajenombrePlayer2.text = 0.toString()
                    puntajenombrePlayer2.setTextColor(Color.parseColor("#da0d00"))
                }else {
                    if (dataSnapshot.child("palabras").child(letra).child("Nombres")
                            .child(
                                dataSnapshot.child("juego").child(roomCreated).child("player2")
                                    .child("nombre").value.toString().trim().toUpperCase()
                            ).value.toString() != "null"
                    ) {


                        if (dataSnapshot.child("juego").child(roomCreated).child("player1")
                                .child("nombre").value.toString().trim().toUpperCase() !=
                            dataSnapshot.child("juego").child(roomCreated).child("player2")
                                .child("nombre").value.toString().trim().toUpperCase()
                        ) {
                            puntajenombrePlayer2.text = 100.toString()
                            puntajenombrePlayer2.setTextColor(Color.parseColor("#10bd00"))
                        } else {
                            puntajenombrePlayer2.text = 50.toString()
                            puntajenombrePlayer2.setTextColor(Color.parseColor("#dabd00"))
                        }

                    } else {
                        puntajenombrePlayer2.text = 0.toString()
                        puntajenombrePlayer2.setTextColor(Color.parseColor("#da0d00"))
                    }
                }


                ciudadPlayer1.text = dataSnapshot.child("juego").child(roomCreated).child("player1")
                    .child("ciudad").value.toString()

                ciudadPlayer2.text = dataSnapshot.child("juego").child(roomCreated).child("player2")
                    .child("ciudad").value.toString()

                if (ciudadPlayer1.text.isNullOrEmpty()){
                    puntajeciudadPlayer1.text=0.toString()
                    puntajeciudadPlayer1.setTextColor(Color.parseColor("#da0d00"))

                }else {
                    if (dataSnapshot.child("palabras").child(letra).child("Ciudades")
                            .child(
                                dataSnapshot.child("juego").child(roomCreated).child("player1")
                                    .child("ciudad").value.toString().trim().toUpperCase()
                            ).value.toString() != "null"
                    ) {


                        if (dataSnapshot.child("juego").child(roomCreated).child("player1")
                                .child("ciudad").value.toString().trim().toUpperCase() !=
                            dataSnapshot.child("juego").child(roomCreated).child("player2")
                                .child("ciudad").value.toString().trim().toUpperCase()
                        ) {
                            puntajeciudadPlayer1.text = 100.toString()
                            puntajeciudadPlayer1.setTextColor(Color.parseColor("#10bd00"))
                        } else {
                            puntajeciudadPlayer1.text = 50.toString()
                            puntajeciudadPlayer1.setTextColor(Color.parseColor("#dabd00"))

                        }

                    } else {
                        puntajeciudadPlayer1.text = 0.toString()
                        puntajeciudadPlayer1.setTextColor(Color.parseColor("#da0d00"))

                    }
                }
                if (ciudadPlayer2.text.isNullOrEmpty()){
                    puntajeciudadPlayer2.text=0.toString()
                    puntajeciudadPlayer2.setTextColor(Color.parseColor("#da0d00"))

                }
else {
                    if (dataSnapshot.child("palabras").child(letra).child("Ciudades")
                            .child(
                                dataSnapshot.child("juego").child(roomCreated).child("player2")
                                    .child("ciudad").value.toString().trim().toUpperCase()
                            ).value.toString() != "null"
                    ) {


                        if (dataSnapshot.child("juego").child(roomCreated).child("player1")
                                .child("ciudad").value.toString().trim().toUpperCase() !=
                            dataSnapshot.child("juego").child(roomCreated).child("player2")
                                .child("ciudad").value.toString().trim().toUpperCase()
                        ) {

                            puntajeciudadPlayer2.text = 100.toString()
                            puntajeciudadPlayer2.setTextColor(Color.parseColor("#10bd00"))
                        } else {
                            puntajeciudadPlayer2.text = 50.toString()
                            puntajeciudadPlayer2.setTextColor(Color.parseColor("#dabd00"))

                        }

                    } else {
                        puntajeciudadPlayer2.setTextColor(Color.parseColor("#da0d00"))
                        puntajeciudadPlayer2.text = 0.toString()

                    }
                }

                paisPlayer1.text = dataSnapshot.child("juego").child(roomCreated).child("player1")
                    .child("pais").value.toString()

                paisPlayer2.text = dataSnapshot.child("juego").child(roomCreated).child("player2")
                    .child("pais").value.toString()

                if (paisPlayer1.text.isNullOrEmpty()){
                    puntajepaisPlayer1.text=0.toString()
                    puntajepaisPlayer1.setTextColor(Color.parseColor("#da0d00"))

                }else {
                    if (dataSnapshot.child("palabras").child(letra).child("Paises")
                            .child(
                                dataSnapshot.child("juego").child(roomCreated).child("player1")
                                    .child("pais").value.toString().trim().toUpperCase()
                            ).value.toString() != "null"
                    ) {


                        if (dataSnapshot.child("juego").child(roomCreated).child("player1")
                                .child("pais").value.toString().trim().toUpperCase() !=
                            dataSnapshot.child("juego").child(roomCreated).child("player2")
                                .child("pais").value.toString().trim().toUpperCase()
                        ) {
                            puntajepaisPlayer1.text = 100.toString()
                            puntajepaisPlayer1.setTextColor(Color.parseColor("#10bd00"))
                        } else {
                            puntajepaisPlayer1.text = 50.toString()
                            puntajepaisPlayer1.setTextColor(Color.parseColor("#dabd00"))

                        }

                    } else {
                        puntajepaisPlayer1.text = 0.toString()
                        puntajepaisPlayer1.setTextColor(Color.parseColor("#da0d00"))

                    }

                }
                if (paisPlayer2.text.isNullOrEmpty()){
                    puntajepaisPlayer2.text=0.toString()
                    puntajepaisPlayer2.setTextColor(Color.parseColor("#da0d00"))

                }else {
                    if (dataSnapshot.child("palabras").child(letra).child("Paises")
                            .child(
                                dataSnapshot.child("juego").child(roomCreated).child("player2")
                                    .child("pais").value.toString().trim().toUpperCase()
                            ).value.toString() != "null"
                    ) {


                        if (dataSnapshot.child("juego").child(roomCreated).child("player1")
                                .child("pais").value.toString().trim().toUpperCase() !=
                            dataSnapshot.child("juego").child(roomCreated).child("player2")
                                .child("pais").value.toString().trim().toUpperCase()
                        ) {


                            puntajepaisPlayer2.setTextColor(Color.parseColor("#10bd00"))
                            puntajepaisPlayer2.text = 100.toString()
                        } else {
                            puntajepaisPlayer2.text = 50.toString()
                            puntajepaisPlayer2.setTextColor(Color.parseColor("#dabd00"))

                        }

                    } else {
                        puntajepaisPlayer2.text = 0.toString()
                        puntajepaisPlayer2.setTextColor(Color.parseColor("#da0d00"))

                    }
                }

                animalPlayer1.text = dataSnapshot.child("juego").child(roomCreated).child("player1")
                    .child("animal").value.toString()

                animalPlayer2.text = dataSnapshot.child("juego").child(roomCreated).child("player2")
                    .child("animal").value.toString()

                if (animalPlayer1.text.isNullOrEmpty()){
                    puntajeanimalPlayer1.text=0.toString()
                    puntajeanimalPlayer1.setTextColor(Color.parseColor("#da0d00"))

                }else {
                    if (dataSnapshot.child("palabras").child(letra).child("Animales")
                            .child(
                                dataSnapshot.child("juego").child(roomCreated).child("player1")
                                    .child("animal").value.toString().trim().toUpperCase()
                            ).value.toString() != "null"
                    ) {


                        if (dataSnapshot.child("juego").child(roomCreated).child("player1")
                                .child("animal").value.toString().trim().toUpperCase() !=
                            dataSnapshot.child("juego").child(roomCreated).child("player2")
                                .child("animal").value.toString().trim().toUpperCase()
                        ) {
                            puntajeanimalPlayer1.text = 100.toString()
                            puntajeanimalPlayer1.setTextColor(Color.parseColor("#10bd00"))
                        } else {
                            puntajeanimalPlayer1.text = 50.toString()
                            puntajeanimalPlayer1.setTextColor(Color.parseColor("#dabd00"))

                        }

                    } else {
                        puntajeanimalPlayer1.text = 0.toString()
                        puntajeanimalPlayer1.setTextColor(Color.parseColor("#da0d00"))

                    }
                }
                if (animalPlayer2.text.isNullOrEmpty()){
                    puntajeanimalPlayer2.text=0.toString()
                    puntajeanimalPlayer2.setTextColor(Color.parseColor("#da0d00"))

                }else {
                    if (dataSnapshot.child("palabras").child(letra).child("Animales")
                            .child(
                                dataSnapshot.child("juego").child(roomCreated).child("player2")
                                    .child("animal").value.toString().trim().toUpperCase()
                            ).value.toString() != "null"
                    ) {


                        if (dataSnapshot.child("juego").child(roomCreated).child("player1")
                                .child("animal").value.toString().trim().toUpperCase() !=
                            dataSnapshot.child("juego").child(roomCreated).child("player2")
                                .child("animal").value.toString().trim().toUpperCase()
                        ) {
                            puntajeanimalPlayer2.text = 100.toString()
                            puntajeanimalPlayer2.setTextColor(Color.parseColor("#10bd00"))
                        } else {
                            puntajeanimalPlayer2.text = 50.toString()
                            puntajeanimalPlayer2.setTextColor(Color.parseColor("#dabd00"))
                        }

                    } else {
                        puntajeanimalPlayer2.text = 0.toString()
                        puntajeanimalPlayer2.setTextColor(Color.parseColor("#da0d00"))
                    }
                }

                frutaPlayer1.text = dataSnapshot.child("juego").child(roomCreated).child("player1")
                    .child("fruta").value.toString()

                frutaPlayer2.text = dataSnapshot.child("juego").child(roomCreated).child("player2")
                    .child("fruta").value.toString()

                if (frutaPlayer1.text.isNullOrEmpty()){
                    puntajefrutaPlayer1.text=0.toString()
                    puntajefrutaPlayer1.setTextColor(Color.parseColor("#da0d00"))

                }else {
                    if (dataSnapshot.child("palabras").child(letra).child("Frutas")
                            .child(
                                dataSnapshot.child("juego").child(roomCreated).child("player1")
                                    .child("fruta").value.toString().trim().toUpperCase()
                            ).value.toString() != "null"
                    ) {


                        if (dataSnapshot.child("juego").child(roomCreated).child("player1")
                                .child("fruta").value.toString().trim().toUpperCase() !=
                            dataSnapshot.child("juego").child(roomCreated).child("player2")
                                .child("fruta").value.toString().trim().toUpperCase()
                        ) {
                            puntajefrutaPlayer1.text = 100.toString()
                            puntajefrutaPlayer1.setTextColor(Color.parseColor("#10bd00"))

                        } else {
                            puntajefrutaPlayer1.text = 50.toString()
                            puntajefrutaPlayer1.setTextColor(Color.parseColor("#dabd00"))
                        }

                    } else {
                        puntajefrutaPlayer1.text = 0.toString()
                        puntajefrutaPlayer1.setTextColor(Color.parseColor("#da0d00"))
                    }
                }
                if (frutaPlayer2.text.isNullOrEmpty()){
                    puntajefrutaPlayer2.text=0.toString()
                    puntajefrutaPlayer2.setTextColor(Color.parseColor("#da0d00"))

                } else {
                    if (dataSnapshot.child("palabras").child(letra).child("Frutas")
                            .child(
                                dataSnapshot.child("juego").child(roomCreated).child("player2")
                                    .child("fruta").value.toString().trim().toUpperCase()
                            ).value.toString() != "null"
                    ) {


                        if (dataSnapshot.child("juego").child(roomCreated).child("player1")
                                .child("fruta").value.toString().trim().toUpperCase() !=
                            dataSnapshot.child("juego").child(roomCreated).child("player2")
                                .child("fruta").value.toString().trim().toUpperCase()
                        ) {
                            puntajefrutaPlayer2.text = 100.toString()
                            puntajefrutaPlayer2.setTextColor(Color.parseColor("#10bd00"))
                        } else {
                            puntajefrutaPlayer2.text = 50.toString()
                            puntajefrutaPlayer2.setTextColor(Color.parseColor("#dabd00"))
                        }

                    } else {
                        puntajefrutaPlayer2.text = 0.toString()
                        puntajefrutaPlayer2.setTextColor(Color.parseColor("#da0d00"))
                    }
                }

                puntajeTotalPlayer1.text = (puntajenombrePlayer1.text.toString().toInt() +
                        puntajeciudadPlayer1.text.toString().toInt() +
                        puntajepaisPlayer1 .text.toString().toInt() +
                        puntajeanimalPlayer1.text.toString().toInt() +
                        puntajefrutaPlayer1.text.toString().toInt() ).toString()

                puntajeTotalPlayer2.text = (puntajenombrePlayer2.text.toString().toInt() +
                        puntajeciudadPlayer2.text.toString().toInt() +
                        puntajepaisPlayer2 .text.toString().toInt() +
                        puntajeanimalPlayer2.text.toString().toInt() +
                        puntajefrutaPlayer2.text.toString().toInt() ).toString()

            }
            override fun onCancelled(databaseError: DatabaseError) {

            }
        }
        ref.addValueEventListener(eventListener)

        nuevaPartida.setOnClickListener {
            val intento1 = Intent(this, SeleccionarLetraActivity::class.java)
            intento1.putExtra(room, roomCreated)
            ref.child("notificado?").setValue("null")

            startActivity(intento1)
        }
        salir.setOnClickListener {
            val intento1 = Intent(this, JoinCreateActivity::class.java)

            startActivity(intento1)
        }
 }

}