package com.example.davcg.prueba

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner

class SeleccionarLetraIndividualActivity : AppCompatActivity() {

   internal lateinit var siguienteB: Button

    lateinit var letter : Spinner
    companion object {
        val INTENT_Letter = "letter"

    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.seleccionar_letra_individual)

        siguienteB = findViewById(R.id.siguienteButton)

        var letra =""
        var notificacion=""
        val playerId1 = "player1"

        letter = findViewById(R.id.letterSpinner) as Spinner
        val letters = arrayOf("A", "B", "C","D", "F", "G", "H", "I", "J", "J", "K", "L", "M", "N", "O", "P", "S", "T", "U", "Z")

        letter.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, letters)

        letter.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                letra=letters.get(position)

            }

        }

        siguienteB.setOnClickListener {
            val intento1 = Intent(this, JuegoIndividualActivity::class.java)
            intento1.putExtra(INTENT_Letter,letra )
            startActivity(intento1)
        }

    }
}
