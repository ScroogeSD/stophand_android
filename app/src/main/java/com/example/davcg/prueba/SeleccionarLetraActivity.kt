package com.example.davcg.prueba
import android.content.Intent
import android.widget.AdapterView
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class SeleccionarLetraActivity:AppCompatActivity() {
    internal lateinit var siguiente: Button
    lateinit var letter :Spinner
    companion object {
        val INTENT_Letter = "letter"
        val INTENT_Player1 = "player1"
        val INTENT_Room ="room"
    }
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        val database = FirebaseDatabase.getInstance()
        val roomCreated = intent.getStringExtra(CrearRoomActivity.INTENT_Room)
        val roomReturned = intent.getStringExtra(PuntajesActivity.room)
        val ref = database.getReference("stopHand/" + "juego/"+roomCreated+"/" )
        ref.child("letra").setValue("null")
        ref.child("parameLaMano2").setValue("null")
        ref.child("notificado?").setValue("null")
        ref.child("parameLaMano1").setValue("null")
        setContentView(R.layout.selecciona_letra)
        siguiente = findViewById(R.id.siguienteButton)
        var letra =""
        var notificacion=""
        val playerId1 = "player1"

        letter = findViewById(R.id.letterSpinner) as Spinner
        val letters = arrayOf("A", "B", "C","D", "F", "G", "H", "I", "J", "J", "K", "L", "M", "N", "O", "P", "S", "T", "U", "Z")
        siguiente.isEnabled=false

        letter.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, letters)

        letter.onItemSelectedListener = object :AdapterView.OnItemSelectedListener{

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
               letra=letters.get(position)
                ref.child("letra").setValue(letra)
            }

        }
        val eventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                notificacion = dataSnapshot.child("notificado?").value.toString()
                    Log.e("notificado",notificacion)

                if (notificacion == "true"){
                    siguiente.isEnabled= true
                }

            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        }
        ref.addValueEventListener(eventListener)

siguiente.setOnClickListener {

  if(roomCreated.isNullOrEmpty()){
      enviar(letra,playerId1,roomReturned)
  }
    else {
      enviar(letra, playerId1, roomCreated)
  }
}
    }
private fun enviar(letra:String, id:String, room:String){
    val intento1 = Intent(this, JuegoActivity::class.java)
    intento1.putExtra(INTENT_Player1,id )

    intento1.putExtra(INTENT_Room, room)
    intento1.putExtra(INTENT_Letter, letra)
    startActivity(intento1)

}


}