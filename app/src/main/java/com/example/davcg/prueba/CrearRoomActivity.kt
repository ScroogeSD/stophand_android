package com.example.davcg.prueba

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.google.firebase.database.FirebaseDatabase
import java.util.*

class CrearRoomActivity:AppCompatActivity() {

    internal lateinit var siguiente: Button
    internal lateinit var room: TextView

    companion object {
        val INTENT_Player1 = "playerId"
        val INTENT_Room = "room"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var r = Random()
        var roomId = r.nextInt(1500 - 1) + 0;

            val database = FirebaseDatabase.getInstance()

        val ref = database.getReference("stopHand/" + "juego/" )
        setContentView(R.layout.crear_room)
        val playerId = intent.getStringExtra(JoinCreateActivity.INTENT_Player);

        siguiente = findViewById(R.id.siguienteButton)
        room=findViewById(R.id.codigoText)
        room.text=roomId.toString()

        siguiente.setOnClickListener {
            val intento1 = Intent(this, SeleccionarLetraActivity::class.java)
            intento1.putExtra(INTENT_Player1, "player1")
            intento1.putExtra(INTENT_Room, room.text.toString())
            startActivity(intento1)

            ref.child(room.text.toString().trim())
                .child("player1")
                .setValue(playerId.toString())


        }

    }
}