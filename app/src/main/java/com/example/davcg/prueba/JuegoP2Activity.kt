package com.example.davcg.prueba

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener



class JuegoP2Activity : AppCompatActivity() {
    companion object {

        var puntajePlayer2 = 0

        var animalPlayer2 = ""

        var frutaPlayer2 = ""

        var paisPlayer2 = ""

        var ciudadPlayer2 = ""

        var nombrePlayer2 = ""
        var letra = ""
        var room = "room"
        var idPlayer2=""

    }

    internal lateinit var letra2TextView: TextView
    internal lateinit var boton: Button
    internal lateinit var nombre: EditText
    internal lateinit var ciudad: EditText
    internal lateinit var pais: EditText
    internal lateinit var animal: EditText
    internal lateinit var fruta: EditText
    var parameLaMano = ""

    internal lateinit var countDownTimer: CountDownTimer
    internal val initialCountDown = 10000L
    internal var countDownInterval = 1000L
    internal var timeLeft = 10
    var playerId2=""
    var roomJoined=""
    val database = FirebaseDatabase.getInstance()
    val ref = database.getReference("stopHand/" + "juego")

    internal var letraDeJuego = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.juegop2_activity)
         playerId2 = intent.getStringExtra(EsperaLetraActivity.INTENT_Player2);


        roomJoined = intent.getStringExtra(EsperaLetraActivity.INTENT_Room)
var stop =""
        var stop2=""
        val letra = intent.getStringExtra(EsperaLetraActivity.INTENT_Letra)
        val ref2 = database.getReference("stopHand/" + "juego/" + roomJoined + "/")

        letra2TextView = findViewById(R.id.letra2TextView)
        boton = findViewById(R.id.button)

        nombre = findViewById(R.id.nameText)
        ciudad = findViewById(R.id.cityText)
        pais = findViewById(R.id.countryText)
        animal = findViewById(R.id.animalText)
        fruta = findViewById(R.id.frutaText)
        letraDeJuego = letra
        letra2TextView.text = letra

        val eventListener = object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                 stop = dataSnapshot.child("parameLaMano2").value.toString()
                 stop2 = dataSnapshot.child("parameLaMano1").value.toString()

                Log.e("stoppy2", stop)

                if (stop != "null" && stop2 != "null") {

                    enviar()
                    countDownTimer.cancel()
                } else if (stop != "null") {
                    contar()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        }
        ref2.addValueEventListener(eventListener)
        boton.setOnClickListener {

                _ ->
            if (stop != "null"){
                enviarABase(playerId2, roomJoined)

                ref.child(roomJoined).child("parameLaMano1").setValue("true")
            }
            else {
                contar()
                enviarABase(playerId2, roomJoined)

                ref.child(roomJoined).child("parameLaMano1").setValue("true")
            }

        }

    }


    private fun enviarABase(playerId: String, room: String) {

        ref.child(room).child(playerId).child("nombre").setValue(nombre.text.toString().trim())
        ref.child(room).child(playerId).child("ciudad").setValue(ciudad.text.toString().trim())
        ref.child(room).child(playerId).child("pais").setValue(pais.text.toString().trim())
        ref.child(room).child(playerId).child("animal").setValue(animal.text.toString().trim())
        ref.child(room).child(playerId).child("fruta").setValue(fruta.text.toString().trim())
    }

    private var isCounting = false

    private fun contar() {

        Log.d("HERE:", "Its been called more than once p2")
        timeLeft = 10

        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval) {
            override fun onFinish() {
                countDownTimer.cancel()

                enviarABase(playerId2, roomJoined)
                enviar()
            }

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                letra2TextView.textSize =14f
                letra2TextView.text = getString(R.string.time_left, Integer.toString(timeLeft))
            }

        }

        if (!isCounting) {
            countDownTimer.start()
            isCounting = true
        }    }

    private fun enviar() {
        countDownTimer.cancel()

        val intento1 = Intent(this, PuntajesP2Activity::class.java)

        intento1.putExtra(idPlayer2, playerId2)
        intento1.putExtra(room, roomJoined)

        intento1.putExtra(JuegoActivity.letra, letraDeJuego)


        startActivity(intento1)

    }

}




