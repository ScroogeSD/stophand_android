package com.example.davcg.prueba

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import android.widget.Button
import com.google.firebase.database.FirebaseDatabase
import java.util.*

class JoinCreateActivity :AppCompatActivity() {
    val database = FirebaseDatabase.getInstance()
    val ref = database.getReference("juego" )
    val partidaId = UUID.randomUUID().toString()
    internal lateinit var create: Button
    internal lateinit var join: Button
    companion object {
        val INTENT_Player = "playerId"
        val INTENT_Partida="partidaId"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.createjoin_activity)
        create = findViewById(R.id.idCreate)
        join = findViewById(R.id.idJoin)

        create.setOnClickListener {
            val intento1 = Intent(this, CrearRoomActivity::class.java)
            intento1.putExtra(INTENT_Player, "1")

            startActivity(intento1)
        }

        join.setOnClickListener {
            val intento1 = Intent(this, UnirseActivity::class.java)
            intento1.putExtra(INTENT_Player, "2")
            startActivity(intento1)
        }
    }
}