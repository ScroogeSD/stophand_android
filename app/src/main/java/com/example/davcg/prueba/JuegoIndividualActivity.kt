package com.example.davcg.prueba

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import java.util.*
import java.util.concurrent.ThreadLocalRandom


class JuegoIndividualActivity : AppCompatActivity() {
    companion object {
        var puntajePlayer = 0

        var animalPlayer = "animal"

        var frutaPlayer = "fruta"

        var paisPlayer = "pais"

        var ciudadPlayer = "ciudad"

        var nombrePlayer = "nombre"

        var letra = "letra"

    }

    internal lateinit var letraTextView: TextView
    internal lateinit var boton: Button
    internal lateinit var nombre: EditText
    internal lateinit var ciudad: EditText
    internal lateinit var pais: EditText
    internal lateinit var animal: EditText
    internal lateinit var fruta: EditText
    internal var letraDeJuego = ""
    internal var nombreP = ""
    internal var ciudadP = ""
    internal var animalP = ""
    internal var paisP = ""
    internal var frutaP = ""

    internal lateinit var countDownTimer: CountDownTimer
    internal lateinit var countDownTimer2: CountDownTimer

    internal var initialCountDown = 10000L
    internal var countDownInterval = 1000L
    internal var timeLeft = 10


     internal var valorDificultad = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.juego_individual)
        val letra1 = intent.getStringExtra(SeleccionarLetraIndividualActivity.INTENT_Letter)

        letraTextView = findViewById(R.id.letraTextView)
        boton = findViewById(R.id.button)

        nombre = findViewById(R.id.nameText)
        ciudad = findViewById(R.id.cityText)
        pais = findViewById(R.id.countryText)
        animal = findViewById(R.id.animalText)
        fruta = findViewById(R.id.frutaText)
        letraDeJuego= letra1
        letraTextView.text = letra1

        val random = Random()



        boton.setOnClickListener {
       countDownTimer.cancel()
            nombreP = nombre.text.toString().toUpperCase().trim()
            ciudadP = ciudad.text.toString().toUpperCase().trim()
            paisP =pais.text.toString().toUpperCase().trim()
            animalP=animal.text.toString().toUpperCase().trim()
            frutaP=fruta.text.toString().toUpperCase().trim()
       enviar()

   }
        valorDificultad  = random.nextInt(1..3)
        contar(valorDificultad)
    }


    fun Random.nextInt(range: IntRange): Int {
        return range.start + nextInt(range.last - range.start)
    }
    private fun contar(tiempo:Int) {
        if (tiempo ===1){
            initialCountDown= 10000L
            timeLeft=10
        }
        if (tiempo ===2){
            initialCountDown= 13000L
            timeLeft=13
        }
        if (tiempo ===3){
            initialCountDown= 17000L
            timeLeft=17
        }




        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval) {
            override fun onFinish() {
                countDownTimer.cancel()
                contador()
            }

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000

            }
        }
        countDownTimer.start()
    }

    private fun contador(){
    countDownTimer2 = object : CountDownTimer(initialCountDown, countDownInterval) {
        override fun onFinish() {
            countDownTimer2.cancel()
            enviar()
        }

        override fun onTick(millisUntilFinished: Long) {
            timeLeft = millisUntilFinished.toInt() / 1000
            letraTextView.textSize=14f
            letraTextView.text = getString(R.string.time_left, Integer.toString(timeLeft))
        }
    }
    countDownTimer2.start()
}
    private fun enviar() {


        countDownTimer.cancel()


        val intento1 = Intent(this, PuntajeIndividualActivity::class.java)
        intento1.putExtra(nombrePlayer, nombreP)
        intento1.putExtra(paisPlayer,paisP )
        intento1.putExtra(ciudadPlayer,ciudadP )
        intento1.putExtra(animalPlayer, animalP )
        intento1.putExtra(frutaPlayer,frutaP )
        intento1.putExtra(letra, letraDeJuego)


        startActivity(intento1)

    }
}
